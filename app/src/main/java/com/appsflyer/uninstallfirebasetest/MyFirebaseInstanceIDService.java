package com.appsflyer.uninstallfirebasetest;


import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by anton on 30/01/2017.
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {


    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("token", "Refreshed token: " + refreshedToken);

    }
}
