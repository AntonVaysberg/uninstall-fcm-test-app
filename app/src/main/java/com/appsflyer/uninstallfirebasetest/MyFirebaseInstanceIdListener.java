package com.appsflyer.uninstallfirebasetest;

import com.appsflyer.AppsFlyerLib;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by anton on 06/02/2017.
 */

public class MyFirebaseInstanceIdListener extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        System.out.println("\n******* Refreshed Token ********* \n"+refreshedToken);
        AppsFlyerLib.getInstance().updateServerUninstallToken(getApplicationContext(),refreshedToken);
        System.out.println("\n******* Refreshed Token ********* \n"+refreshedToken);
    }

}
